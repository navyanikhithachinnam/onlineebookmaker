package com.book.store.dao;

import com.book.store.models.BookRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRequestDAO extends JpaRepository<BookRequest, Long> {
    List<BookRequest> findByBook_User_Id(Long userId);
}
